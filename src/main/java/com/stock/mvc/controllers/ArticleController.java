package com.stock.mvc.controllers;

import java.util.ArrayList;
import java.util.List;

import org.junit.runner.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.entites.Article;
import com.stock.mvc.services.IArticleService;

@Controller
@RequestMapping(value="/article")
public class ArticleController {
	
	@Autowired
	private IArticleService articleService;
	
	@RequestMapping(value="/")
	public String article(Model model){
		List<Article> articles = articleService.selectAll();
		if (articles == null)
			articles = new ArrayList<Article>();
		model.addAttribute("articles",articles);	
		return "article/article";
	}
	@RequestMapping(value="/nouveau", method=RequestMethod.GET)
	public String ajouterArticle(Model model){
		Article article = new Article();
		//client.getNom();
		model.addAttribute("article",article);
		return "article/ajouterArticle";
	}
	
	@RequestMapping(value="/Enregistrer", method=RequestMethod.POST)
	public String enregistrerArticle(Model model, Article article) throws Exception {
		if(article != null){
			if (article.getIdArticle() != null){
				articleService.update(article);
			}
			else
				articleService.save(article);
			
		}
		return "redirect:/article/";
	}
	@RequestMapping(value="/supprimer/{idArticle}")
	public String supprimerArticle(Model model, @PathVariable Long idArticle){
		if(idArticle != null){
			Article article = articleService.getById(idArticle);
			if (article != null){
				//TO DO vérification avant suppressio car on peut pas supprimer un client qui a des commande en cours ...
				articleService.remove(idArticle);
			}
		}
		return "redirect:/article/";
	}
	
	@RequestMapping(value="/modifier/{idArticle}")
	public String modifierArticle(Model model, @PathVariable Long idArticle){
		if (idArticle != null) {
			Article article= articleService.getById(idArticle);
			if (article != null){
				model.addAttribute("article",article);
			}
		}
		return "article/ajouterArticle";
	}

}
