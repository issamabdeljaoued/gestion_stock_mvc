package com.stock.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.stock.mvc.utils.ApplicationUtils;

@Controller
@RequestMapping(value="/home")
public class HomeController {
	
	@RequestMapping(value="/")
	public String home(){
		//ApplicationUtils.changeLocale(request, response, "FR");
		return "home/home";
	}
	
	@RequestMapping(value="/blank")
	public String blankhome(){
		return "blank/blank";
		
		
	}

}
