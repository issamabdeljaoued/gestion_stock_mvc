package com.stock.mvc.controllers;

import java.util.ArrayList;
import java.util.List;

import org.junit.runner.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.entites.Category;
import com.stock.mvc.services.ICategoryService;

@Controller
@RequestMapping(value="/category")
public class CategoryController {
	
	@Autowired
	private ICategoryService categoryService;
	
	@RequestMapping(value="/")
	public String category(Model model){
		List<Category> categorys = categoryService.selectAll();
		if (categorys == null)
			categorys = new ArrayList<Category>();
		model.addAttribute("categorys",categorys);	
		return "category/category";
	}
	@RequestMapping(value="/nouveau", method=RequestMethod.GET)
	public String ajouterCategory(Model model){
		Category category = new Category();
		//client.getNom();
		model.addAttribute("category",category);
		return "category/ajouterCategory";
	}
	
	@RequestMapping(value="/Enregistrer", method=RequestMethod.POST)
	public String enregistrerCategory(Model model, Category category) throws Exception {
		if(category != null){
			if (category.getIdCategory() != null){
				categoryService.update(category);
			}
			else
			categoryService.save(category);
			
		}
		return "redirect:/category/";
	}
	@RequestMapping(value="/supprimer/{idCategory}")
	public String supprimerCategory(Model model, @PathVariable Long idCategory){
		if(idCategory != null){
			Category category = categoryService.getById(idCategory);
			if (category != null){
				//TO DO vérification avant suppressio car on peut pas supprimer un client qui a des commande en cours ...
				categoryService.remove(idCategory);
			}
		}
		return "redirect:/category/";
	}
	
	@RequestMapping(value="/modifier/{idCategory}")
	public String modifierCategory(Model model, @PathVariable Long idCategory){
		if (idCategory != null) {
			Category category = categoryService.getById(idCategory);
			if (category != null){
				model.addAttribute("category",category);
			}
		}
		return "category/ajouterCategory";
	}

}
