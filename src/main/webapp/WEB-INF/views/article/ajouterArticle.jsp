<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="fr">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Gestion de Stock MVC</title>

<!-- Bootstrap Core CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link
	href="<%=request.getContextPath()%>/resources/dist/css/sb-admin-2.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">

			<!-- /.navbar-top-links -->
			<%@ include file="/WEB-INF/views/menu_top/topMenu.jsp"%>

			<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp"%>
			<!-- /.navbar-static-side -->
		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Blank</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<spring:message code="article.nouveau" />
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
							<c:url value="/article/Enregistrer" var="urlEnregistrer" />
								<f:form modelAttribute="article" action="${urlEnregistrer }" method="Post">
									<div> 
										<f:input type="hidden" path="idArticle" placeholder="idArticle" />
									</div>
									<div class="form-group">
										<label><spring:message code="article.code" /></label> 
										<f:input path="codeArticle" class="form-control" placeholder="Code" />
									</div>
									<div class="form-group">
										<label><spring:message code="article.designation" /></label> 
										<f:input path="designation" class="form-control" placeholder="Designation" />
									</div>
									<div class="form-group">
										<label><spring:message code="article.prix" /></label> 
										<f:input path="prixUnitaireHT" class="form-control" placeholder="Prix HT" />
									</div>
									<div class="form-group">
										<label><spring:message code="article.tauxTva" /></label> 
										<f:input path="tauxTva" class="form-control" placeholder="Taux TVA" />
									</div>
									<div class="form-group">
										<label><spring:message code="article.ttc" /></label> 
										<f:input path="prixUnitaireTTC" class="form-control" placeholder="Prix TTC" />
									</div>
									<div class="pannel-footer">
									<button type="submit" class="btn btn-primary"><spring:message code="common.enregistrer" /></button>
									<a href="<c:url value="/article/"/>" class="btn btn-danger"><spring:message code="common.annuler" /></a>
									</div>
								</f:form>
								<!-- /.table-responsive -->

							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="../vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/dist/js/sb-admin-2.js"></script>

</body>

</html>
